import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { AuthGuardGuard } from './auth-guard.guard';
import { AuthentificationComponent } from './authentification/authentification.component';
import { ComptableGuard } from './comptable.guard';
import { ComptableComponent } from './comptable/comptable-vue-tab.component';
import { ConsulterNotesComponent } from './consulter-notes/consulter-notes.component';
import { SaisieNotesComponent } from './saisie-notes/saisie-notes.component';

const routes: Routes = [

  { path: '', component: AuthentificationComponent },

  {
    path: 'saisie', component: SaisieNotesComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'saisie/:idVisiteur/:mois', component: SaisieNotesComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'consulter', component: ConsulterNotesComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'accueil', component: AccueilComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'validation', component: ComptableComponent,
    canActivate: [AuthGuardGuard,ComptableGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
