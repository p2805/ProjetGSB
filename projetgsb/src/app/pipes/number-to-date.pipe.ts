import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberToDate'
})
export class NumberToDatePipe implements PipeTransform {

  transform(value: string, ...args: any[]): any {
    const annee = value.slice(0, 4)
    const mois = value.slice(4)

    return mois + '-' + annee;
  }

}
