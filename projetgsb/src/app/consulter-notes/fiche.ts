export interface Fiche {
    idVisiteur: string;
    mois: number;
    nbJustificatifs: number;
    montantValide: number;
    dateModif: string;
    idEtat: string;
    libelle:string;
  }
  

  