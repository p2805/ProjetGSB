export interface HorsForfait {
    libelle: string,
    date?: Date,
    montant: string
  }