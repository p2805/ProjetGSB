import { TestBed } from '@angular/core/testing';

import { ComptableGuard } from './comptable.guard';

describe('ComptableGuard', () => {
  let guard: ComptableGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ComptableGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
