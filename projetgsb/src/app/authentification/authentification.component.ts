import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from './user';
import { UserService } from './user.service';

@Component({
  selector: 'authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.css']
})
export class AuthentificationComponent implements OnInit {
  error = '';
  success = '';
  login="";
  mdp="";
  con:any;

  constructor(private userService: UserService,
    private router : Router) { }

  ngOnInit(): void {
  }



    newUser(): void {
      this.userService.postNewUser(this.login,this.mdp).subscribe(
        (data: User) => {
           this.con=data.id;
        },
        (err) => {
          console.log(err);
          this.error = err;
        }
      );
    }
logout(){
  this.userService.logout().subscribe((success)=>{
    console.log("Déconnecter");

  })


}
  }



