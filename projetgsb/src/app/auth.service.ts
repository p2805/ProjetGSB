import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  isLoggedIn() {
    const username = localStorage.getItem('username'); // get token from local storage
    console.log(username)
    return !!username;
  }

  getUserId() {
    return localStorage.getItem('user_id');
  }
  getUserNom() {
    return localStorage.getItem('user_nom');
  }
  getUserPrenom() {
    return localStorage.getItem('user_prenom');
  }

  getSessionId() {
    return localStorage.getItem('sessionId');
  }
  
  isComptable() {
    return localStorage.getItem('user_comptable') == "1";
  }
}
