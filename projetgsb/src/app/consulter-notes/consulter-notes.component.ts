import { Component, OnInit } from '@angular/core';
import { Fiche } from './fiche';
import { FicheService } from './ficheService';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'consulter-notes',
  templateUrl: './consulter-notes.component.html',
  styleUrls: ['./consulter-notes.component.css']
})
export class ConsulterNotesComponent implements OnInit {

  baseUrl = environment.api_url; 
  data=[];
  fiche: Fiche[] = [];
  displayedColumns : any[] = ['mois', 'justificatifs', 'montant', 'dateModif', 'etat', 'actions']

  constructor(
    private ficheService: FicheService, 
    private authService: AuthService,
    private http: HttpClient,
    private router:Router) { }

  ngOnInit(): void {
    this.getFiche();
  }
  

  getFiche(): void {
    this.ficheService.getAll().subscribe(
      (data: Fiche[]) => {
        this.fiche = data;
        this.data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  supprimer() {
    return  this.http.delete(`${this.baseUrl}deleteFiche`, {withCredentials: true}).subscribe((success) =>{
      console.log("Suppression réussie");
    },
    (error) =>{
      console.log(error, 'erreur');
    })
}

}

