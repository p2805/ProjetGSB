import { Pipe, PipeTransform } from '@angular/core';
import { AuthService } from '../auth.service';

@Pipe({
  name: 'prenomNom'
})
export class PrenomNomPipe implements PipeTransform {

  constructor(private authService: AuthService) { }

  transform(value: unknown, ...args: unknown[]): any {
    value = this.authService.getUserPrenom() + " " + this.authService.getUserNom()?.toUpperCase();
    return value
  }

}
