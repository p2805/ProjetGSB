import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../authentification/user.service';
import {MatIconModule} from '@angular/material/icon';
import { AuthService } from '../auth.service';
@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  con:any;
  constructor(private userService: UserService,
    public authService: AuthService,
    private router : Router) { }

  ngOnInit(): void {
  }
  logout(){
    this.userService.logout().subscribe((success)=>{
    })
  }
}
