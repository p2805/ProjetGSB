import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { UserService } from '../authentification/user.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  nom="";

  constructor(public authService: AuthService,private userService: UserService,) { }

  ngOnInit(): void {
  }
  logout(){
    this.userService.logout().subscribe((success)=>{
    })
  }
}

