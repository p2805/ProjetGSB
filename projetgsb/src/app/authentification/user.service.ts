import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.api_url;

  constructor(private http: HttpClient,
    private router:Router) { }


  getAll() {
    return this.http.get(`${this.baseUrl}list`,{withCredentials: true}).pipe(
      map((res: any) => {
        return res['data'];
      })
    );
  }

  postNewUser(login:string,mdp:String) {
    return this.http.post<any>(`${this.baseUrl}auth`, JSON.stringify({ username: login, password: mdp }), )
    .pipe(map(dataFromApi => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        if(dataFromApi.id == 1) {
          //La connection a reussi
          localStorage.setItem('username', login);
          localStorage.setItem('user_id', dataFromApi.user_id);
          localStorage.setItem('user_prenom', dataFromApi.user_prenom);
          localStorage.setItem('user_nom', dataFromApi.user_nom);
          localStorage.setItem('sessionId', dataFromApi.sessionId)
          localStorage.setItem('user_comptable', dataFromApi.user_comptable ? "1" : "0");
          console.log('user_id', dataFromApi.user_id,dataFromApi.user_nom,dataFromApi.user_prenom);
          this.router.navigate(['/accueil']);
        }
        return dataFromApi;
    }));
  }

  logout(){
    return this.http.post<any>(`${this.baseUrl}logout`,{})
    .pipe(map(user => {
          localStorage.removeItem('username');
          localStorage.removeItem('user_id');
          localStorage.removeItem('user_prenom');
          localStorage.removeItem('user_nom');
          localStorage.removeItem('sessionId');

          this.router.navigate(['/']);
        return user;
    }));
  }
}

