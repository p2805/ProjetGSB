import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(
    private router : Router,
    private AuthService: AuthService
    ) {
    }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      if (!this.AuthService.isLoggedIn()) {
        console.log("not connected");
        this.router.navigate(['/']); // go to login if not authenticated
        return false;
      }
      console.log("connected")
        return true;
  }


}
