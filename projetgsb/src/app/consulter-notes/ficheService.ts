import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth.service';
@Injectable({
  providedIn: 'root',
})
export class FicheService {
  baseUrl = environment.api_url;

  constructor(private http: HttpClient,
    private authService: AuthService) {}

  getAll() {
    return this.http.get(`${this.baseUrl}consulterfichetab`,{headers:{PHPSESSID: this.authService.getSessionId() || ""}}).pipe(
      map((res: any) => {
        return res['data'];
      })
    );
  }
}
