import { Component, OnInit } from '@angular/core';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import { Fiche } from '../consulter-notes/fiche';
import { ComptableService } from '../comptable/comptable.service';

@Component({
  selector: 'app-comptable',
  templateUrl: './comptable-vue-tab.component.html',
  styleUrls: ['./comptable-vue-tab.component.css']
})
export class ComptableComponent implements OnInit {
  data=[];
  ficheATraiter: Fiche[] = [];
  ficheTraitees: Fiche[] = [];
  displayedColumns : any[] = ['id', 'mois', 'justificatifs', 'montant', 'dateModif', 'etat', 'actions']
  dataSource = new MatTableDataSource( );

  constructor( public comptableService:ComptableService, ) { }

  ngOnInit(): void {
    this.getFicheATraiter();
    this.getFicheTraitees();
  }
  getFicheATraiter(): void {
    this.comptableService.getFicheEtat().subscribe(
      (data: Fiche[]) => {

        this.ficheATraiter = data.filter(d => d.idEtat !== 'RB');
        this.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  getFicheTraitees(): void {
    this.comptableService.getFicheEtat().subscribe(
      (data: Fiche[]) => {

        this.ficheTraitees = data.filter(d => d.idEtat == 'RB');
        console.log(data)
        this.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }
}
