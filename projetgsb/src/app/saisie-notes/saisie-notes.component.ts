import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { SnackBarService } from '../service/snackbar.service';
import { formatDate } from '@angular/common';
import { FicheFrais } from './entites/ficheFrais';
import { Forfait } from './entites/forfait';
import { HorsForfait } from './entites/horsForfait';
import { AuthService } from '../auth.service';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Fiche } from '../consulter-notes/fiche';
import { map, pluck, Subscription } from 'rxjs';

@Component({
  selector: 'saisie-notes',
  templateUrl: './saisie-notes.component.html',
  styleUrls: ['./saisie-notes.component.css']
})
export class SaisieNotesComponent implements OnInit {
  elemFiche: FicheFrais;
  elemForfait: Forfait;
  elemHorsForfait: HorsForfait;
  baseUrl = environment.api_url;
  moisEnCours: number = Date.now();
  readonly: boolean = false;
  nom = "";
  montantEtape: string = "";
  montantKm: string = "";
  montantNuit: string = "";
  montantRepas: string = "";

  constructor(
    private http: HttpClient,
    private snackBarService: SnackBarService,
    public authService: AuthService,
    private activatedRoute: ActivatedRoute) {
    this.elemFiche = {
      mois: formatDate(Date.now(), 'YYYYMM', 'fr-FR'),
      nbJustificatifs: "0",
      montantValide: "0",
      dateModif: new Date(),
      idEtat: ""
    }
    this.elemHorsForfait = {
      libelle: " ",
      date: undefined,
      montant: "0"
    }
    this.elemForfait = {
      etape: "0",
      kilometre: "0",
      nuit: "0",
      repas: "0",
      montant: 0
    }

    let idVisiteur = this.activatedRoute.snapshot.params['idVisiteur'];
    let mois = this.activatedRoute.snapshot.params['mois'];
    if (mois && idVisiteur) {
      this.initialiserFormulaireFiche(idVisiteur, mois);
    }
  }

  initialiserFormulaireFiche(idVisiteur: string, mois: string) {
    console.log(idVisiteur, mois);
    this.authService.getUserNom()
    this.http.get(`${this.baseUrl}consulterfichedetails`, {
      headers: { PHPSESSID: this.authService.getSessionId() || "" },
      params: { mois: mois, idVisiteur: idVisiteur }
    }).subscribe((data: any) => {
      if (data.ficheFrais) {
       if (data.ficheFrais.idEtat !== 'CR' && !this.authService.isComptable) {
          this.readonly = true;
        }
        if ((data.ficheFrais.idEtat == 'RB' || data.ficheFrais.idEtat == 'VA') && 
        this.authService.isComptable()) {
          this.readonly = true;
        }
        //On peut mettre à jour formulaire supérieure
        this.elemFiche.mois = data.ficheFrais.mois;
        this.elemFiche.nbJustificatifs = data.ficheFrais.nbJustificatifs;
        this.elemFiche.dateModif = data.ficheFrais.dateModif
        this.elemFiche.montantValide = data.ficheFrais.montantValide;
        this.elemFiche.idEtat = data.ficheFrais.idEtat;
        //   this.elemFiche.etat = data.ficheFrais.etat

      }

      if (data.forfait) {
        this.elemForfait.etape = data.forfait.ETP && data.forfait.ETP.quantite || 0;
        this.montantEtape = data.forfait.ETP && data.forfait.ETP.total || 0;
        this.elemForfait.kilometre = data.forfait.KM && data.forfait.KM.quantite || 0;
        this.montantKm = data.forfait.KM && data.forfait.KM.total || 0;
        this.elemForfait.nuit = data.forfait.NUI && data.forfait.NUI.quantite || 0;
        this.montantNuit = data.forfait.NUI && data.forfait.NUI.total || 0;
        this.elemForfait.repas = data.forfait.REP && data.forfait.REP.quantite || 0;
        this.montantRepas = data.forfait.REP && data.forfait.REP.total || 0;

        console.log(data.forfait.KM, data.forfait)
      }

      if (data.horsForfait) {
        this.elemHorsForfait.libelle = data.horsForfait.libelle;
        this.elemHorsForfait.date = data.horsForfait.date;
        this.elemHorsForfait.montant = data.horsForfait.montant;
      }
    })
  }

  ngOnInit(): void {

  }

  saveForfait() {
    let formData = new FormData();
    if (this.authService.isComptable()) {
      formData.append("idVisiteur", this.activatedRoute.snapshot.params['idVisiteur']);
    } else {
      formData.append("idVisiteur", this.getUserId());
    }
    formData.append("mois", this.elemFiche.mois);
    formData.append("qteEtape", this.elemForfait.etape);
    formData.append("qteKilometre", this.elemForfait.kilometre);
    formData.append("qteNuit", this.elemForfait.nuit);
    formData.append("qteRepas", this.elemForfait.repas);

    return this.http.post(`${this.baseUrl}lignefraisforfait`, formData, { withCredentials: true })
  }

  saveHForfait() {
    let formData = new FormData();
    if (this.authService.isComptable()) {
      formData.append("idVisiteur", this.activatedRoute.snapshot.params['idVisiteur']);
    } else {
      formData.append("idVisiteur", this.getUserId());
    }
    formData.append("mois", this.elemFiche.mois);
    formData.append("libelle", this.elemHorsForfait.libelle || " ");
    formData.append("montant", this.elemHorsForfait.montant);
    if(this.elemHorsForfait.date) {
      formData.append("date", '' + this.elemHorsForfait.date.valueOf());
    }
    return this.http.post(`${this.baseUrl}lignefraishorsforfait`, formData, { withCredentials: true })
  }
/*pb format date si aucune date entrée et pb datemodif qui ne s'actualise pas*/
  save() {
    let formData = new FormData();
    if (this.authService.isComptable()) {
      formData.append("montantValide", this.getMontantTotal());
      formData.append("idVisiteur", this.activatedRoute.snapshot.params['idVisiteur']);
    } else {
      formData.append("montantValide", this.elemFiche.montantValide);
      formData.append("idVisiteur", this.getUserId());
    }
    formData.append("mois", this.elemFiche.mois);
    formData.append("nbJustificatifs", this.elemFiche.nbJustificatifs);
    formData.append("dateModif", "" + this.elemFiche.dateModif.valueOf());
    formData.append("idEtat", this.elemFiche.idEtat || "CR");
    return this.http.post(`${this.baseUrl}fichefrais`, formData, { withCredentials: true })
  }

  sauvegarde() {
    this.save().subscribe((success1) => {
      this.saveForfait().subscribe((success2) => {
        this.saveHForfait().subscribe((success3) => {
          this.snackBarService.openSnackBar('Votre fiche a bien été enregistrée', 'Fermer');
          console.log("Enregistrement réussi");
        },
          (error) => {
            console.log(error, 'Troisieme requête');
          })
      },
        (error) => {
          console.log(error, 'deuxième requête');
        })
    }, (error) => {
      console.log(error, 'première requête');
    })
  }

  getUserId() {
    return this.authService.getUserId() || "";
  }

  getMontantTotal() {
    return this.montantEtape + this.montantKm + this.montantNuit + this.montantRepas + parseInt(this.elemHorsForfait.montant);
  }


}
