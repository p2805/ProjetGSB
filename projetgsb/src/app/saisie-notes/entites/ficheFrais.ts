export interface FicheFrais {
    mois: string,
    nbJustificatifs: string,
    montantValide: string,
    dateModif: Date,
    idEtat: string
  }