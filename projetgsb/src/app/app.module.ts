import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {MatMenuModule} from '@angular/material/menu';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaisieNotesComponent } from './saisie-notes/saisie-notes.component';
import { ConsulterNotesComponent } from './consulter-notes/consulter-notes.component';
import { AuthentificationComponent } from './authentification/authentification.component';
import { NavigationComponent } from './navigation/navigation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {HttpClientModule} from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import { MatNativeDateModule } from '@angular/material/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AccueilComponent } from './accueil/accueil.component';
import {MatIconModule} from '@angular/material/icon';
import { NumberToDatePipe } from './pipes/number-to-date.pipe';
import {MatTooltipModule} from '@angular/material/tooltip';
import { ComptableComponent } from './comptable/comptable-vue-tab.component';
import { PrenomNomPipe } from './pipes/prenom-nom.pipe';
import { MatPaginatorModule } from '@angular/material/paginator';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    SaisieNotesComponent,
    ConsulterNotesComponent,
    AuthentificationComponent,
    NavigationComponent,
    AccueilComponent,
    NumberToDatePipe,
    ComptableComponent,
    PrenomNomPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatSidenavModule,
    MatButtonModule,
    HttpClientModule,
    MatMenuModule,
    MatTableModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatIconModule,
    MatTooltipModule,
    MatPaginatorModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'}, 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
